---
title: Structural Calculation Report - Milwaukee St 4429
author: Aether Engineering | Ioannis Christovasilis, Eng., PhD
date: August 25, 2023
---

<header>
    <div>
        <img src="aether_logo.png" alt="Aether Engineering logo" style="width: 286px; height: 110px;">
    </div>
    <div class="contact-info">
        <p style="font-size: 18px">AETHER ENGINEERING</p>
        <p>Via Masaccio 252, 50132, Florence, ITALY</p>
        <p>VAT: IT06587990489 | TEL: +1 347 809 3370</p>
        <p><a href="mailto:info@aethereng.com">info@aethereng.com</a> | <a href="https://www.aethereng.com">www.aethereng.com</a></p>
    </div>
</header>
<div style="border-top: 1px solid grey;"></div>

<div class="main_title">
    Structural Calculation Report - Milwaukee St 4429
</div>

<div style="border-top: 1px solid grey;"></div>

<div>
  <div class="main_container">
      <div class="column">
          <p>CLIENT</p>
          <p>TITLE</p>
          <p>VERSION</p>
          <p>DATE</p>
      </div>
      <div>
          <p>OpeningDesign</p>
          <p>Structural Calculation Report – Milwaukee St 4429</p>
          <p>01</p>
          <p>25 – August – 2023</p>
      </div>
  </div>
</div>

<div style="border-top: 1px solid grey;"></div>

<!-- Page Break -->
<div style="page-break-before: always;"></div>
<header>
    <div>
        <img src="aether_logo.png" alt="Aether Engineering logo" style="width: 286px; height: 110px;">
    </div>
    <div class="contact-info">
        <p style="font-size: 18px">AETHER ENGINEERING</p>
        <p>Via Masaccio 252, 50132, Florence, ITALY</p>
        <p>VAT: IT06587990489 | TEL: +1 347 809 3370</p>
        <p><a href="mailto:info@aethereng.com">info@aethereng.com</a> | <a href="https://www.aethereng.com">www.aethereng.com</a></p>
    </div>
</header>
<div style="border-top: 1px solid grey;"></div>


### 1. Introduction
This report presents the structural calculations needed for the realization of two new apertures in an external light-frame wood shear wall at the first floor of the building situated at 4429 Milwaukee St, Madison, WI 53714. More specifically, calculations are presented for the dimensioning of the header and the jack studs of the apertures.

### 2.	Design Codes
The following Design Codes have been considered for the analysis and design:
- ASCE/SEI 7-10: Minimum Design Loads for Buildings and Other Structures
- Wisconsin Building Code 2015
- 2018 National Design Specification (NDS) for Wood Construction

### 3. Structure
The structure is a two-story building with mixed commercial and residential use. <a href="#figure1">Fig. 1</a> shows a plan view and indicates the main length measures considered in the design. These numbers are conservative with respect to the actual dimensions to justify the simplified analysis methods employed for the specification of the acting forces in the new to-be-designed members.

<div style="text-align: center;">
    <figure>
        <img id="figure1" src="plan_view.png" alt="Plan view" style="width: 440px; height: 450px; margin-right: auto;">
        <figcaption><i>Fig. 1: Plan view of the first floor of the structure, indicating the total width and the length of the aperture.</i></figcaption>
    </figure>
</div>

<p style="text-align: center; margin-top: 30px"><i>page 2 of 5</i></p>

<!-- Page Break -->
<div style="page-break-before: always;"></div>
<header>
    <div>
        <img src="aether_logo.png" alt="Aether Engineering logo" style="width: 286px; height: 110px;">
    </div>
    <div class="contact-info">
        <p style="font-size: 18px">AETHER ENGINEERING</p>
        <p>Via Masaccio 252, 50132, Florence, ITALY</p>
        <p>VAT: IT06587990489 | TEL: +1 347 809 3370</p>
        <p><a href="mailto:info@aethereng.com">info@aethereng.com</a> | <a href="https://www.aethereng.com">www.aethereng.com</a></p>
    </div>
</header>
<div style="border-top: 1px solid grey;"></div>

### 4 Loads
#### 4.1 Load Analysis
The header of the aperture has 3 sources of distributed vertical loads:
1. From the floor of the second story with a tributary width of 20 ft;
2. From the roof of the second story with a tributary width of 20 ft;
3. From the external wall above the header with a height of 10 ft.


The following nominal loads have been considered:
- Dead Load (**D**):
  -	20 psf, for the floor of the second story;
  - 20 psf, for the roof of the second story;
  - 15 psf, for the external wall above the beam
- Live Load (**L**):
  - 40 psf, for the floor of the second story;
- Snow Load (**S**):
  - 30 psf, for the inclined roof (conservative value)

With these considerations, the final linearly distributed loads acting on the header are:
- **D**: (20 psf + 20 psf) x 20 ft + 15 psf x 10 ft = 950 plf
- **L**: 40 psf x 20 ft = 800 plf
- **S**: 30 psf x 20 ft = 600 plf

#### 4.2 Load Combinations
Considering Allowable Stress Design (ASD), the following load combinations were evaluated for the Ultimate Limit States:

1. **D**                              <span style="padding-left: 150px">--> Q<sub>dis</sub> = 950 plf</span> with C<sub>D</sub> = 0.9
2. **D** + **L**			          <span style="padding-left: 130px">--> Q<sub>dis</sub> = 1750 plf</span> with C<sub>D</sub> = 1.0
3. **D** + **S**			          <span style="padding-left: 130px">--> Q<sub>dis</sub> = 1550 plf</span> with C<sub>D</sub> = 1.15
4. **D** + 0.75 x (**L** + **S**)	  <span style="padding-left: 66px">--> Q<sub>dis</sub> = 2000 plf</span> with C<sub>D</sub> = 1.15

where Q<sub>dis</sub> is the distributed design load and C<sub>D</sub> is the Load Duration Factor

### 4 Header Design
#### 4.1 Geometry and Materials

Three (3) 2x8 sistered members made of Southern Pine, visually graded as **No. 2**, are considered for the verification with the geometric and material properties as shown in <a href="#table1">Table 1</a>.


<figcaption id="table1"><i>Table 1: Geometric and material properties of the header.</i></figcaption>

| Section | width (in) | height (in) | A (in²) | S (in³) | I (in⁴) | F<sub>b</sub> (psi) | F<sub>v</sub> (psi) | F<sub>c⊥</sub> (psi) | E (ksi) |
|---------|------------|-------------|---------|---------|---------|---------------------|---------------------|----------------------|---------|
| (3) 2x8 | 4.5        | 7.25        | 32.62   | 39.42   | 142.9   | 1380                | 175                 | 480                  | 1400    |

<p style="text-align: center; margin-top: 30px"><i>page 3 of 5</i></p>

<!-- Page Break -->
<div style="page-break-before: always;"></div>
<header>
    <div>
        <img src="aether_logo.png" alt="Aether Engineering logo" style="width: 286px; height: 110px;">
    </div>
    <div class="contact-info">
        <p style="font-size: 18px">AETHER ENGINEERING</p>
        <p>Via Masaccio 252, 50132, Florence, ITALY</p>
        <p>VAT: IT06587990489 | TEL: +1 347 809 3370</p>
        <p><a href="mailto:info@aethereng.com">info@aethereng.com</a> | <a href="https://www.aethereng.com">www.aethereng.com</a></p>
    </div>
</header>
<div style="border-top: 1px solid grey;"></div>

#### 4.2 Design and Verification for Ultimate Limit States
The header is analyzed as a simply-supported beam with length L = 4'-4" = 52 in.

The bending moment at the center is shown in <a href="#table2">Table 2</a> for each combination along with the stress resultants and strength resistances for each case. The design bending strength is equal to the nominal value times the Load Duration Factor
(F<sub>bd</sub> = F<sub>b</sub> ꞏ C<sub>D</sub>).

<figcaption id="table2"><i>Table 2: Bending moment design and verification.</i></figcaption>

| Combination        | Moment (lbs-in) | f<sub>b</sub> (psi) | F<sub>bd</sub> (psi) | f<sub>b</sub> ≤ F<sub>bd</sub> |
|--------------------|-----------------|---------------------|----------------------|--------------------------------|
| 1: D               | 26758.4         | 678.8               | 1242                 | ✔                             |
| 2: D + L           | 49291.7         | 1250.4              | 1380                 | ✔                             |
| 3: D + S           | 43658.4         | 1107.5              | 1587                 | ✔                             |
| 4: D + 0.75(L + S) | 56333.3         | 1429.0              | 1587                 | ✔                             |

The shear force at the support is shown in <a href="#table3">Table 3</a> for each combination along with the stress resultants and strength resistances for each case. The design shear strength is equal to the nominal value times the Load Duration Factor
(F<sub>vd</sub> = F<sub>v</sub> ꞏ C<sub>D</sub>).

<figcaption id="table3"><i>Table 3: Shear force design and verification.</i></figcaption>

| Combination        | Shear (lbs) | f<sub>b</sub> (psi) | F<sub>vd</sub> (psi) | f<sub>b</sub> ≤ F<sub>vd</sub> |
|--------------------|-------------|---------------------|----------------------|--------------------------------|
| 1: D               | 2058.4      | 94.6                | 157.5                | ✔                             |
| 2: D + L           | 3791.7      | 174.3               | 175                  | ✔                             |
| 3: D + S           | 3358.4      | 154.4               | 201.2                | ✔                             |
| 4: D + 0.75(L + S) | 4333.4      | 199.2               | 201.2                | ✔                             |

The bearing force at the support is shown in <a href="#table4">Table 4</a> for each combination along with the stress resultants and bearing capacity for each case. The bearing length is taken considering two (2) 2x6 jack studs (A<sub>c⊥</sub> = 4.5 in ꞏ 3.0 in = 13.5 in²) with C<sub>b</sub> = 1.

<figcaption id="table4"><i>Table 4: Bearing design and verification perpendicular-to-grain.</i></figcaption>

| Combination        | Bearing (lbs) | f<sub>c⊥</sub> (psi) | F<sub>c⊥</sub> (psi) | f<sub>c⊥</sub> ≤ F<sub>c⊥</sub> |
|--------------------|---------------|----------------------|----------------------|---------------------------------|
| 1: D               | 2058.4        | 152.5                | 480                  | ✔                              |
| 2: D + L           | 3791.7        | 280.9                | 480                  | ✔                              |
| 3: D + S           | 3358.4        | 248.8                | 480                  | ✔                              |
| 4: D + 0.75(L + S) | 4333.4        | 321.0                | 480                  | ✔                              |

<p style="text-align: center; margin-top: 50px"><i>page 4 of 5</i></p>

<!-- Page Break -->
<div style="page-break-before: always;"></div>
<header>
    <div>
        <img src="aether_logo.png" alt="Aether Engineering logo" style="width: 286px; height: 110px;">
    </div>
    <div class="contact-info">
        <p style="font-size: 18px">AETHER ENGINEERING</p>
        <p>Via Masaccio 252, 50132, Florence, ITALY</p>
        <p>VAT: IT06587990489 | TEL: +1 347 809 3370</p>
        <p><a href="mailto:info@aethereng.com">info@aethereng.com</a> | <a href="https://www.aethereng.com">www.aethereng.com</a></p>
    </div>
</header>
<div style="border-top: 1px solid grey;"></div>

#### 4.2 Design and Verification for Serviceability

A short-term deflection limit of L / 400 is considered for load combination #2.

Δ<sub>ST</sub> = 5 ꞏ Q<sub>dis</sub> ꞏ L<sup>4</sup> / (384 ꞏ EI) = 5 ꞏ 145.9 ꞏ 52<sup>4</sup> / (384 ꞏ 1.4E6 ꞏ 142.9) = 0.07 in < 0.13 in = L / 400 = 52 / 400 ✔

### 5 Stud Design
#### 5.1 Geometry and Materials

Two (2) 2x6 sistered members made of Southern Pine, visually graded as **No. 2**, are considered for the verification of the jack studs, with the geometric and material properties as shown in <a href="#table5">Table 5</a>.

<figcaption id="table5"><i>Table 5: Geometric and material properties of the jack studs.</i></figcaption>

| Section | width (in) | height (in) | A (in²) | F<sub>c</sub> (psi) | E<sub>min</sub> (ksi) |
|---------|------------|-------------|---------|---------------------|-----------------------|
| (2) 2x6 | 3.0        | 5.5         | 16.5    | 1400                | 510                   |

#### 5.2 Design and Verification for Ultimate Limit States

The height of the studs is H = 6'-6" = 78 in. Buckling is considered to occur only along the strong axis (bending out of wall plane) as the nailing of the OSB panels to the studs is providing stability for in-plane bending.
Thus, the slenderness ratio, considering the effective length to be the full stud height, is L<sub>e</sub> / d = 78 / 5.5 = 14.2.

In order to calculate the Column Stability Factor C<sub>P</sub> from Eq. (3.7-1) of the 2018 NDS Specifications we have:
- F<sub>cE</sub> = 0.8222 ꞏ E<sub>min</sub> / (L<sub>e</sub> / d)² = 0.8222 ꞏ 510000 / 14.2² = 2085 psi
- c = 0.8 for sawn lumber
- F<sub>c</sub><sup>*</sup> equal to the reference compression design value parallel-to-grain excluding the effect of C<sub>P</sub>

The axial compressive force, taken equal to the bearing force at the support of the header, is shown in <a href="#table6">Table 6</a> for each combination along with the stress resultants and strength resistances for each case. The reference compression parallel-to-grain is equal to the nominal value times the Load Duration Factor (F<sub>c</sub><sup>*</sup> = F<sub>c</sub> ꞏ C<sub>D</sub>).

<figcaption id="table6"><i>Table 6: Axial compression design and verification.</i></figcaption>

| Combination        | Compression (lbs) | f<sub>c</sub> (psi) | F<sub>c</sub><sup>*</sup> (psi) | C<sub>P</sub> | F<sub>cd</sub> (psi) | f<sub>c</sub> ≤ F<sub>cd</sub> |
|--------------------|-------------------|---------------------|---------------------------------|---------------|----------------------|--------------------------------|
| 1: D               | 2058.4            | 124.8               | 1260                            | 0.86          | 1083.9               | ✔                             |
| 2: D + L           | 3791.7            | 229.8               | 1400                            | 0.83          | 1173.4               | ✔                             |
| 3: D + S           | 3358.4            | 203.5               | 1610                            | 0.80          | 1293.1               | ✔                             |
| 4: D + 0.75(L + S) | 4333.4            | 262.6               | 1610                            | 0.80          | 1293.1               | ✔                             |

<p style="text-align: center; margin-top: 100px"><i>page 5 of 5</i></p>

<style>

/* Your custom CSS styles go here */
@font-face {
    font-family: CenturyGothic;
    src: url(ufonts.com_century-gothic.ttf);
}

header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px;
    padding-bottom: 0px;
}

.contact-info {
    line-height: 10px;
    text-align: right;
}

.main_title {
    margin-top: 250px;
    margin-bottom: 250px;
    color: rgb(54, 53, 53);
    font-family: inherit;
    font-weight: 300;
    font-size: 22px;
    text-align: center;
}

.main_container {
    display: flex;
    line-height: 10px;
    margin-left: 50px;
    margin-top: 15px;
    margin-bottom: 15px;
}

.column {
    width: 150px;
}

a {
    color: #428bca;
    text-decoration: none;
}

blockquote {
    background-color: rgb(128, 128, 128, 0.05);
    border-bottom-right-radius: 5px;
    border-left-width: 5px;
    border-top-right-radius: 5px;
    padding: 10px 15px;
    font-size: 12px;
}

body {
    background-color: #fff;
    color: rgb(54, 53, 53);
    font-family: 'CenturyGothic', 'Lato', sans-serif;
    font-size: 12px;
    line-height: 1.5;
    text-align: justify;
}

h1,
.h1 {
    font-size: 20px;
}

h1,
h2,
h3,
h4,
h5,
h6,
.h1,
.h2,
.h3,
.h4,
.h5,
.h6 {
    color: rgb(54, 53, 53);
    font-family: inherit;
    font-weight: 300;
    margin: 15px;
    margin-bottom: 5px;
    margin-right: 10px;
    margin-left: 0;
    margin-top: 10px;
}

h1:after,
h2:after {
    border-bottom: none;
    text-decoration: none;
}

h2,
.h2 {
    font-size: 18px;
    margin-bottom: 30px;
    margin-top: 30px;
}

h3,
.h3 {
    font-size: 16px;
    margin-top: 20px;
    text-transform: uppercase;
    font-weight: bold;
}

h4,
.h4 {
    font-size: 14px;
    font-variant: small-caps;
    font-weight: bold;
}

h5,
.h5 {
    font-size: 12px;
    margin-top: 0;
}

h6,
.h6 {
    font-size: 10px;
}

hr {
    border-bottom: none;
    border-left: none;
    border-right: none;
    border-top: 0.5px dashed rgb(177, 177, 177);
    margin-bottom: 25px;
    margin-top: 25px;
}

/* ol {
    list-style-type: upper-roman;
} */

table th,
table td {
    border: 1px solid #999;
    line-height: 20px;
    padding: 8px;
    vertical-align: center;
}

code {
    color: #111;
    background-color: #ddd;
    border-radius: 4px;
    font-family: 'Courier New', Courier, monospace;
    padding: 2px 4px;
}

</style>